function getSum(str1, str2) {

  if (typeof (str1) != "string" || typeof (str2) != "string") {
    return false;
  }

  let FirstNumber = Number(str1);
  let SeconNumber = Number(str2);

  if (isNaN(FirstNumber) || isNaN(SeconNumber)) {
    return false;
  }
  return (FirstNumber + SeconNumber).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost = 0, countComments = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if(listOfPosts[i].author == authorName)
      {
        countPost++;
      }
    if(listOfPosts[i].comments != undefined)
    {
      for (let j = 0; j < listOfPosts[i].comments.length; j++) {
        if(listOfPosts[i].comments[j].author == authorName)
        {
          countComments++;
        }    
      }
    }
  }
  return "Post:"+ countPost+ ",comments:" +countComments;
}

function tickets(people) {
  let change25 = 0, change50 = 0;
  for (let i = 0; i < people.length; i++) {
    if (people[0] != 25 && people[0] != 50) {
      return "NO";
    }
    if (people[i] == 25) {
      change25 += 1;
    }
    if (people[i] == 50) {
      change25 -= 1;
      change50 += 1;
    }
    if (people[i] == 100) {
      if (change50 == 0 && change25 == 3)
        change25 -= 3;

      else {
        change25 -= 1;
        change50 -= 1;
      }
    }
    if (change25 < 0 || change50 < 0) {
      return "NO";
    }
  }
  return "YES";
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
